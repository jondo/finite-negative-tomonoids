finite-negative-tomonoids
=========================

This C++ program computes all one-element Rees coextensions of a given finite negative tomonoid.
Based on this method, it is e.g. possible to count the number of negative tomonoids of a given size.

This software was created by Laura Peham in the framework of the bilateral Austrian-Czech project
[New Perspectives at Residuated Posets](https://flll.jku.at/respos) 2015 - 2017 at the
[Department of Knowledge-Based Mathematical Systems](https://flll.jku.at) of
[Johannes Kepler University Linz](http://www.jku.at), supervised by
[Thomas Vetterlein](https://www.flll.jku.at/staff/vetterlein) and
[Milan Petrík](http://home.czu.cz/petrikm/),
with technical support by [Robert Pollak](https://www.flll.jku.at/staff/pollak).

In scientific publications, please refer to it as:
> L. Peham, finite-negative-tomonoids, 2017, https://gitlab.com/jondo/finite-negative-tomonoids

The implementation is based on the following publications:
> M. Petrík, T. Vetterlein Rees coextensions of finite, negative
tomonoids, J. Log. Comput. 27 (2017), 337 - 356.
>
> M. Petrík, T. Vetterlein, An algorithm for generating finite
totally ordered monoids, in: J.P. Carvalho, M.-J. Lesot, U. Kaymak, S.
Vieira, B. Bouchon-Meunier, R.R. Yager, Information Processing and
Management of Uncertainty in Knowledge-Based Systems, Proceedings of
IPMU (Juni 2016, Eindhoven), Springer-Verlag 2016; 532 - 543.


License
-------

*finite-negative-tomonoids* is licensed under the GPL (version 3 or later).

Build
-----
This software was built with [Qt Creator 3.4.2](https://download.qt.io/archive/qt/5.5/5.5.0/qt-opensource-windows-x86-mingw492-5.5.0.exe.mirrorlist).

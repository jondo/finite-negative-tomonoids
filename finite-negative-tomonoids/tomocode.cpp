/* tomocode.cpp
 *
 * Copyright 2015-2017 Johannes Kepler University Linz,
 * Department of Knowledge Based Mathematical Systems,
 * author: Laura Peham <l.peham@hotmail.com>.
 *
 * This file is part of finite-negative-tomonoids.
 *
 * finite-negative-tomonoids is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * finite-negative-tomonoids is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with finite-negative-tomonoids. If not, see <http://www.gnu.org/licenses/>.
 */

#include "tomocode.h"
#include "tomomath.h"
#include "relation.h"
#include<iostream>
#include<set>
#include<vector>
using namespace std;


// Default Constructor
// Constructs a two-dimensional tomonoid  with values 0 and 1
TomoCode::TomoCode()
{
    tom.resize(4);
    n=2;
    tom[0]=0;
    tom[1]=1;
    tom[2]=1;
    tom[3]=1;

}

// Constructor
// takes a tomonoid in form of TomoMath
// Converts the TomoMath to a TomoCode
TomoCode::TomoCode(TomoMath t_m)
{
    tom.resize(t_m.n*t_m.n);
    n=t_m.n;
    for(int k=1; k<n; k++)
    {
        for(int i=1; i<n; i++)
        {
            for(int j=n-2; j>=0; j--)
            {
                if(t_m.tom[(i*n)+j]==t_m.tom[(1*n)+n-k-1])
                    this->tom[(i*n)+n-1-j]=k;
            }
        }
    }
    for(int i=0; i<n; i++)
        this->tom[i]= i;
    for(int i=0; i<n; i++)
        this->tom[i*n]= i;
}


// Constructor
// takes a Relation
// converts the relation to a tomocode
TomoCode::TomoCode(Relation vector)
{
    tom.resize(((vector.p)+2)*((vector.p)+2));
    n=((vector.p)+2);
    for(int i=0; i<vector.sets.size(); i++)
    {
        for(int j=0; j<vector.sets.size(); j++)
        {
            if((vector.sets.at(j)).count(Point(0,i)))
            {
                set<Point> s0;
                s0=vector.sets.at(j);
                set<Point>::iterator it;
                for(it=s0.begin(); it!=s0.end(); it++)
                {
                    tom[((it->x)*n)+(it->y)]=i;
                }
            }
        }
    }

    id.x=vector.idempotents.at(0);  //so after the coextension, these are the used (el, er)
    id.y=vector.idempotents.at(1);
}


// Destructor
TomoCode::~TomoCode()
{
    tom.clear();
}




// creates the zero-doubling extension of the tomonoid
// returns this zero doubling extension
// the underlying tomonoid will remain unchanged
TomoCode TomoCode::extend()
{
    TomoCode extended;
    extended.tom.resize((n+1)*(n+1));
    extended.n=(n+1);
    for(int i=0; i<n; i++)
       {
        for(int j=0; j<n; j++)
            extended.tom[(i*(extended.n))+j]=this->tom[(i*(this->n))+j];
    }
    for(int i=0; i<n+1; i++)
        extended.tom[(i*(n+1))+n]=n;
    for(int j=0; j<n; j++)
        extended.tom[((n+1)*n)+j]=n;
    return extended;
}




//checks, if tomocode a is equal to this tomocode
//returns true, if it is; false if it isn't
bool TomoCode::operator==(TomoCode const &a) const
{
    int test=1;
    vector<int>::const_iterator it1=tom.begin();
    vector<int>::const_iterator it2=(&a)->tom.begin();
    while((it1<this->tom.end())&&(it2<(&a)->tom.end()))
    {
        if(((&a)->tom.at(*it2))!=(this->tom.at(*it1)))
            test=0;
        it1++;
        it2++;

    }

    if(this->n==(&a)->n)
    {
        if(test==1)
            return true;
        else
            return false;
    }
    else
        return false;
}



// checks if a is bigger than this TomoCode, i.e.is has more elements or bigger elements
bool TomoCode::operator<(TomoCode const &a) const
{
    if(n<(&a)->n)
        return true;
    else
    {
        for(int i=1; i<n; i++)
        {
            for(int j=1; j<n; j++)
            {
                if((this->tom.at(i*n+j))>((&a)->tom.at(i*n+j)))
                    return true;
                else if((this->tom.at(i*n+j))<((&a)->tom.at(i*n+j)))
                     return false;
            }
        }
    }
    return false;
}




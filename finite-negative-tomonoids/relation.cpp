/* relation.cpp
 *
 * Copyright 2015-2017 Johannes Kepler University Linz,
 * Department of Knowledge Based Mathematical Systems,
 * author: Laura Peham <l.peham@hotmail.com>.
 *
 * This file is part of finite-negative-tomonoids.
 *
 * finite-negative-tomonoids is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * finite-negative-tomonoids is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with finite-negative-tomonoids. If not, see <http://www.gnu.org/licenses/>.
 */

#include "relation.h"

void merge(set<Point> &a, set<Point> &b);



//Default Constructor
//makes sets and idempotents empty with size 0, sets p=0
Relation::Relation()
{
    sets.resize(0);
    p=0;
    idempotents.resize(0);
}


//Constructor
//takes a tomonoid in form of tomocode
//and converts it in a relation which includes its equivalent classes
Relation::Relation(TomoCode t_c)
{
    int n=t_c.n; //number of elements in t_c, just not to have to type in t_c.n every time
    idempotents.reserve(n); //reserves t_c.n places for the vector
    for(int i=0; i<n; i++) //inserts the idempotent elements of t_c in the vector 'idempotents'
    {
        if(t_c.tom[(i*n)+i]==t_c.tom[i])
            idempotents.insert(idempotents.end(), i);
    }

    p=n-2;  // p is the size of the tomonoid minus the elements 0 and alpha

    sets.reserve((n-1)*(n-1));  //reserves space for the vector sets

    set<Point> s1, s2; //s1 is the set of alpha, s2 is the set of 0

    for(int i=0; i<n-1; i++)
    {
        for(int j=0; j<n-1; j++)
        {
            if(t_c.tom[(i*n)+j]<p) // the equivalences in P
            {
                if((!(this->has(Point(i,j)))))   //if it isn't in any set yet
                {
                    set<Point> s;
                    for(int k=0; k<n-1; k++)  //searches for all the points equivalent to (i,j), inserts them in the set
                    {
                        for(int l=0; l<n-1; l++)
                        {
                            if((t_c.tom[(i*n)+j]==t_c.tom[(k*n)+l]))
                                s.insert(s.end(), Point(k,l));
                        }
                    }
                    sets.push_back(s);  //inserts the set in 'sets'
                }
            }

            else  // the sets containing the elements of Q except 0
            {
                if(!((i==0 && j==p)||(i==p && j==0))) // not the elements (0,alpha) and (alpha,0)
                {
                    set<Point> s;
                    s.insert(s.end(), Point(i,j));
                    sets.push_back(s);
                }
            }
         }
    }

    s1.insert(s1.end(),Point(p,0));   //inserts the elements in the set of alpha
    s1.insert(s1.end(),Point(0,p));
    for(int i=0; i<n; i++)    //inserts the elements in the set of 0
        s2.insert(s2.end(), Point(i, p+1));
    for(int i=0; i<n; i++)
        s2.insert(s2.end(),Point(p+1, i));
    sets.push_back(s1);  //inserts set of alpha and 0 in 'sets'
    sets.push_back(s2);
}




// checks if Point x is in the vector
// returns true, if it is
// returs false, if it isn't
bool Relation::has(Point x)
{
    for(size_t i=0; i<sets.size(); i++)
    {
        if((sets.at(i)).count(x))
            return (sets.at(i)).count(x);
    }
    return false;
}


// checks if Point a is in P
// returns true, if it is
// returs false, if it isn't
bool Relation::inP(Point a)
{
    if(this->getClass(a)<p)
        return true;
    else
        return false;
}


//returns the element, in which equivalence class the element 'a' is
//if a is not equivalent to any element in the first row or firts column, it returns 100
//if a is not in the set, it returns 1000
int Relation::getClass(Point a)
{
    if(a.x==0) //checks if the point is in the first row or first column (then it is easy)
    {
        return a.y;
    }
    if(a.y==0)
    {
        return a.x;
    }
    for(size_t i=0; i<sets.size(); i++)  // checks in which set the point is
    {
        if((sets.at(i)).count(a))
        {
            for(int j=0; j<p+2; j++)
            {
                if(sets.at(i).count(Point(0,j))) //checks to which element in the first row point a is equivalent
                    return j;
            }
            return 100; //if it is in no equivalence class, i.e. if it's class contains no element of the form (0,x)
        }
    }
    return 1000; //if the point is not in any class
}



// gives back the set of points which include point a
set<Point>& Relation::getSet(Point a)
{
    for(size_t i=0; i<sets.size(); i++)
    {
        if((sets.at(i)).count(a))
        {
            return sets.at(i);
        }
    }
}


//performs E2
void Relation::E2()
{
    for(int a=0; a<p; a++)
    {
        for(int b=0; b<p; b++)
        {
            for(int c=0; c<p; c++)
            {
                if((inP(Point(a,b)))&&(inP(Point(b,c)))&&(!((a==b)&&(b==c))))  //checks, if (a,b) and (b,c) are in P, and if not a=b=c
                {
                    int d=getClass(Point(a,b));
                    int e=getClass(Point(b,c));
                    merge(getSet(Point(a,e)), getSet(Point(d,c)));
                }
            }
        }
    }
}



void Relation::E3a(int el, int er)
{
    for(int a=p-1; a>el; a--)
    {
        for(int b=0; b<p+2; b++)
        {
            if(!inP(Point(a,b)))
            {
                int c=er+1;
                int e=getClass(Point(b,c));
                if(e<p)
                    merge(getSet(Point(p+1,0)), getSet(Point(a,e)));
                else
                    break;
            }
        }
    }

    for(int c=p-1; c>er; c--)
    {
        for(int b=0; b<p+2; b++)
        {
            if(!inP(Point(b,c)))
            {
                int a=el+1;
                int d=getClass(Point(a,b));
                if(d<p)
                    merge(getSet(Point(p+1,0)), getSet(Point(d,c)));
                else
                    break;
            }
        }
    }

}



void Relation::E3b(int el, int er, int id_low)
{
    for(int b=p-1; b>id_low; b--)
    {
        if(inP(Point(b,er)))
        {
            int e=getClass(Point(b,er));
            if(e>b)
            {
                for(int a=p-1; a>el; a--)
                {
                    if(!inP(Point(a,b)))
                        merge(getSet(Point(a,e)), getSet(Point(a,b)));
                }
            }
        }
    }

    for(int b=p-1; b>id_low; b--)
    {
        if(inP(Point(el,b)))
        {
            int d=getClass(Point(el,b));
            if(d>b)
            {
                for(int c=p-1; c>er; c--)
                {
                    if(!inP(Point(b,c)))
                        merge(getSet(Point(d,c)), getSet(Point(b,c)));
                }
            }
        }
    }
}


void Relation::E3c(int el, int er)
{
    int done;
    for(int c=er; c>0; c--)
    {
        done=0;

        for(int b=0; b<p+2; b++)
        {
            if(done==0)
            {
                if(!inP(Point(b,c)))
                {
                    for(int a=0; a<p+2; a++)
                    {
                        if(done==0)
                        {
                            if((!inP(Point(a,b)))&&(a>el))
                            {
                                merge(getSet(Point(p+1,0)),getSet(Point(a,b)));
                                done=1;
                            }
                        }
                    }
                }
            }
        }
    }


    for(int a=el; a>0; a--)
    {
        done=0;

        if(done==0)
        {
            for(int b=0; b<p+2; b++)
            {
                if(!inP(Point(a,b)))
                {
                    if(done==0)
                    {
                        for(int c=0; c<p+2; c++)
                        {
                            if((!inP(Point(b,c)))&&(c>er))
                            {
                                merge(getSet(Point(p+1,0)),getSet(Point(b,c)));
                                done=1;
                            }
                        }
                    }
                }
            }
        }
    }
}

 //performs E3
void Relation::E3(int el, int er, int id_low)
{
    E3a(el, er);
    E3c(el, er);
    E3b(el, er, id_low);
}


//performs E4
void Relation::E4(int el, int er)
{
    merge(getSet(Point(0,p)), getSet(Point(el,p))); //merges the sets (el, alpha) and (alpha, er) with the set of alpha
    merge(getSet(Point(0,p)), getSet(Point(p,er)));

    for(int a=p; a>el; a--)  //merges (a, alpha) and (alpha, b) with 0, for a<el and b<er
        merge(getSet(Point(p+1,0)), getSet(Point(a,p)));
    for(int b=p; b>er; b--)
        merge(getSet(Point(p+1,0)), getSet(Point(p,b)));
}



//closes the sets of alpha and 0,
//so every element bigger than one in the alpha class is also in the alpha class
// and every element smaller than one in the 0 class is also in the 0 class
void Relation::close()
{
    // first for 0:
    for(int h=0; h<p; h++)
    {
        set<Point>& set0 = this->getSet(Point(0,p+1));   //the equivalenceclass of 0
        set<Point>::iterator it;
        for(it=set0.begin(); it != set0.end(); it++)
        {
            if((it->x != p+1) && (it->y != p+1))   //if it is no element in the row or column of 0
            {
                int j=it->y;
                for(int i=it->x; i<=p; i++)
                    merge(getSet(Point(0,p+1)), getSet(Point(i,j)));   //includes all points beneath the one the iterator is at

                int k=it->x;
                for(int l=it->y; l<=p; l++)
                    merge(getSet(Point(0,p+1)), getSet(Point(k,l)));   //includes all points right to the one the iterator is at
            }
        }
    }


    //now for alpha
    for(int h=0; h<p; h++)   //so that also the diagonal-elements are checked
    {
        set<Point>::iterator it;
        set<Point>& setA = this->getSet(Point(0,p));  //the equivalenceclass of alpha
        for(it=setA.begin(); it!= setA.end(); it++)
        {
            int j=it->y;
            for(int i=it->x; i>0; i--)
            {
                if((getClass(Point(i,j)) == 100) || getClass(Point(i,j)) == 0)  //if the point isn't in P
                    merge(getSet(Point(0,p)), getSet(Point(i,j)));   //includes all points above the one the iterator is at
            }

            int k=it->x;
            for(int l=it->y; l>0; l--)
            {
                if((getClass(Point(k,l)) == 100) || getClass(Point(k,l)) == 0)  //if the point isn't in P
                    merge(getSet(Point(0,p)), getSet(Point(k,l)));   //includes all points left to the one the iterator is at
            }
        }
    }
}



//deletes all the empty sets in the vector 'sets', which developed from merging
void Relation::deleteEmpty()
{
    vector<set<Point> >::iterator it=sets.begin();
    while(it!=sets.end())
    {
        if((*it).empty())
            it=sets.erase(it);
        else
            it++;
    }
}





// gives back a vector of relations, each relation is a ramification of the tomonoid with every possible choice of (el, er)
// the idempotents in the new relation is the chosen el and er.
list<Relation> Relation::ramification()
{
    list<Relation> ramifications;  //the vector where the new relations are saved in

    int el; //the idempotent el
    int er;  //the idempotent er
    int n=idempotents.size();
    int id_low=idempotents.back(); //the lowest non-zero idempotent

    for(int i=0; i<n; i++) //chooses every possible set of (el, er)
    {
        el=idempotents.at(i);
        for(int j=0; j<n; j++)
        {
            er=idempotents.at(j);

            Relation rami;
            rami.sets=this->sets;
            rami.p=this->p;

            rami.E2();
            rami.E4(el,er);
            rami.E3(el,er, id_low);

            rami.close();
            rami.deleteEmpty();


            //in the vector 'idempotents' now el and er will be saved
            rami.idempotents.clear();
            rami.idempotents.reserve(2);
            (rami.idempotents).push_back(el);
            (rami.idempotents).push_back(er);

            if(rami.getClass(Point(p+1, p+1)) != rami.getClass(Point(p, 0)))  //if the set of 0 is not the set of alpha, it was sucessful
                ramifications.push_back(rami);
        }
    }
    return ramifications;
}




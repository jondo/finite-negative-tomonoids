/* point.cpp
 *
 * Copyright 2015-2017 Johannes Kepler University Linz,
 * Department of Knowledge Based Mathematical Systems,
 * author: Laura Peham <l.peham@hotmail.com>.
 *
 * This file is part of finite-negative-tomonoids.
 *
 * finite-negative-tomonoids is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * finite-negative-tomonoids is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with finite-negative-tomonoids. If not, see <http://www.gnu.org/licenses/>.
 */

#include"point.h"

//Default Constructor
//generates the Point (0,0)
Point::Point()
{
    x=0;
    y=0;
}


//Constructor
//generates the Point (a,b)
Point::Point(int a, int b)
{
    x=a;
    y=b;
}



//checks, if point a is bigger than this point
//returns true, if it is; false if it isn't
// Point a bigger means that both x and y of a are bigger
bool Point::operator<(Point a) const
{
    if(this->x < a.x)
        return true;
    else if(this->x == a.x)
    {
        if(this->y < a.y)
            return true;
        else
            return false;
    }
    else
        return false;
}



//checks, if point a is equal to this point
//returns true, if it is; false if it isn't
bool Point::operator==(Point a) const
{
    if(this->x != a.x)
        return false;
    else
    {
        if(this->y != a.y)
            return false;
        else
            return true;
    }
}


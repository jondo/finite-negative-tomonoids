TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    tomomath.cpp \
    tomocode.cpp \
    sidefunctions.cpp \
    point.cpp \
    relation.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    tomomath.h \
    tomocode.h \
    point.h \
    relation.h


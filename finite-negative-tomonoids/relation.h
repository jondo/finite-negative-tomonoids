/* relation.h
 *
 * Copyright 2015-2017 Johannes Kepler University Linz,
 * Department of Knowledge Based Mathematical Systems,
 * author: Laura Peham <l.peham@hotmail.com>.
 *
 * This file is part of finite-negative-tomonoids.
 *
 * finite-negative-tomonoids is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * finite-negative-tomonoids is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with finite-negative-tomonoids. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RELATION_H
#define RELATION_H

#include<vector>
#include<set>
#include<list>
#include"point.h"
#include"tomocode.h"
using namespace std;
#include<iostream>


//The class relation represents a Tomonoid splitted in it's equivalence classes
//The main member is a vector<set<Point>> sets, where every set of Points represents an equivalence class
//Other members are p-the number of elements in P- and a vector<int> idempotents, where all the idempotent elements are saved in

class Relation
{
public:
    vector<set<Point> > sets;    //each set represents an equivalenceclass, the vector contains them all
    int p; //number of elements in P, i.e. the element on place p is alpha
    vector<int> idempotents; //the non-zero idempotent elements of the tomonoid

    Relation();
    Relation(TomoCode t_c);  // converts a tomonoid of type tomocode in an element of type relation

    bool has(Point x);  //checks, if Point x is in the vector<set>
    bool inP(Point a);  //checks, if Point a is in P
    int getClass(Point a);  //returns the element, in which equivalence class the element a is
    set<Point>& getSet(Point a);  //returns the whole equivalence class of an element a
    void E2();  // performs E2
    void E3(int el, int er, int id_low);  //performs E3a-E3c
    void E3a(int el, int er);
    void E3b(int el, int er, int id_low);
    void E3c(int el, int er);
    void E4(int el, int er);  // performs E4
    void close();  //closes the set of alpha and 0, i.e. every element below and on the left of a 0-element is 0; and above and right from a alpha-element is alpha
    list<Relation> ramification();   //executes E2-E4 with all possible combinations of idempotents, saves the new relations in a vector
    void deleteEmpty();   //delets the empty sets in the vector 'sets', which developed from merging
};

#endif // RELATION_H

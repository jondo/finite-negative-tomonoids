/* tomomath.cpp
 *
 * Copyright 2015-2017 Johannes Kepler University Linz,
 * Department of Knowledge Based Mathematical Systems,
 * author: Laura Peham <l.peham@hotmail.com>.
 *
 * This file is part of finite-negative-tomonoids.
 *
 * finite-negative-tomonoids is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * finite-negative-tomonoids is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with finite-negative-tomonoids. If not, see <http://www.gnu.org/licenses/>.
 */

#include "tomomath.h"
#include "relation.h"
#include<iostream>
#include<fstream>
#include<algorithm>
using namespace std;

list<Relation> coarsening(list<Relation> &worklist);


// Default Constructor
// Constructs a two-dimensional tomonoid  with values 0 and 1
TomoMath::TomoMath()
{
   tom.resize(4);
   n=2;
   this->tom[0]= '0';
   this->tom[1]= '1';
   this->tom[2]= '0';
   this->tom[3]= '0';

}

// Constructor
// takes int number, which is the size of the tomonoid
// lets the user set the values
TomoMath::TomoMath(int number)
{
    tom.resize(number*number);
    n=number;
    cout<<"Tomonoid of size "<< number<< ":"<<endl;
    cout<<"Type in the elements row to row, press 'Enter' after each element."<< endl;
    for(int i=0; i<n; i++)
    {
        for(int j=0; j<n; j++)
        {
            cin>>this->tom[(i*n)+j];
        }
    }
    cout<<endl;
}


// Constructor
// takes a tomonoid in form of TomoCode
// converts the TomoCode in TomoMath
TomoMath::TomoMath(TomoCode t_c)
{
    tom.resize((t_c.n)*(t_c.n));
    n=t_c.n;
    char alph[]= {'1','z','y','x','w','v','u','t','s','r','q','p','o','n'};
    for(int i=0; i<n-1; i++)
    {
        for(int j=1; j<n; j++)
        {
            if((t_c.tom[(i*n)+n-j-1])==n-1)
                this->tom[(i*n)+j]='0';
            else
                this->tom[(i*n)+j]=alph[(t_c.tom[(i*n)+n-j-1])];
        }
    }
    for(int i=0; i<n; i++)
        this->tom[i*n]= '0';
    for(int i=1; i<n; i++)
        this->tom[((n-1)*n)+i]= '0';

    // the idempotents
    if(t_c.id.x==n-1)
        this->id_x=0;
    else
        this->id_x=alph[t_c.id.x];

    if(t_c.id.y==n-1)
        this->id_y=0;
    else
        this->id_y=alph[t_c.id.y];



}


// Destructor
TomoMath::~TomoMath()
{
    tom.clear();

}




// reads in the tomonoid from a txt file
// structure of the text file: fist the number of elements in the tomonoid, then the tomonoid row to row.
void TomoMath::input(const char* filename)
{
    ifstream file;
    file.open(filename, ios_base::in);
    if(!file.is_open())
        cout<<"File could not be read."<< endl;
    else
    {
        int number;
        file>>number;
        tom.resize(number*number);
        n=number;
        for(int i=0; i<n; i++)
        {
            for(int j=0; j<n; j++)
                file>>this->tom[(i*n)+j];
        }
    }
    file.close();
}


// prints the tomonoid
void TomoMath::write()
{
    for(int i=0; i<n; i++)
        {
        for(int j=0; j<n; j++)
            cout<< this->tom[(i*n)+j]<<" ";

        cout<<endl;
        }
}


//this is the final function in the program
//it operates on a TomoMath, and gives back a list of TomoMath
//in the list all the coextensions of the original TomoMath are saved
list<TomoMath> TomoMath::getCoextensions()
{
    TomoCode t_c(*this);
    TomoCode extended = t_c.extend();
    Relation r(extended);
    list<Relation> rami = r.ramification();
    list<Relation> coars = coarsening(rami);
    rami.clear(); //unnötig?


    set<TomoCode> tomoList;
    list<Relation>::iterator it;
    for(it=coars.begin(); it!=coars.end(); it++)
    {
        if((tomoList.count(TomoCode(*it)))==0)
            tomoList.insert(tomoList.end(), TomoCode(*it));
    }
    coars.clear();
    list<TomoMath> finalList;
    set<TomoCode>::iterator iter;
    for(iter=tomoList.begin(); iter!=tomoList.end(); iter++)
    {
        finalList.push_back(TomoMath(*iter));
    }
    tomoList.clear();

    return finalList;
}








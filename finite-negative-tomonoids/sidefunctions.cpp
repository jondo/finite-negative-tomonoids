/* sidefunctions.cpp
 *
 * Copyright 2015-2017 Johannes Kepler University Linz,
 * Department of Knowledge Based Mathematical Systems,
 * author: Laura Peham <l.peham@hotmail.com>.
 *
 * This file is part of finite-negative-tomonoids.
 *
 * finite-negative-tomonoids is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * finite-negative-tomonoids is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with finite-negative-tomonoids. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <set>
#include "tomocode.h"
#include "tomomath.h"
#include "relation.h"

using namespace std;

//merges two sets of points, set b is inserted in set a, set be is cleared
void merge(set<Point> &a, set<Point> &b)
{
    if(a!=b) //checks if the sets aren't equal yet
    {
        set<Point>::iterator i;
        for(i=b.begin(); i!=b.end(); i++)
            a.insert(a.end(), *i);
        b.clear();
    }
}

// closes the set of 0 in the coarsening
// so it takes the Point which was newly included to the class of 0
// and merges all elements beneath and right to it with the 0-class
Relation close0(Point a, Relation r)
{
    for(int i=a.x; i<=r.p; i++)
    {
        for(int j=a.y; j<=r.p; j++)
        {
            merge(r.getSet(Point(r.p+1, 0)), r.getSet(Point(i,j)));
        }
    }
    return r;
}


// closes the set of alpha in the coarsening
// so it takes the Point which was newly included to the class of alpha
// and merges all elements (not in P) above and left to it with the alpha-class
Relation closeA(Point a, Relation r)
{
    for(int i=a.x; i>0; i--)
    {
        for(int j=a.y; j>0; j--)
        {
            if(r.getClass(Point(i,j)) == 100)  //if the point isn't in P
            {
                merge(r.getSet(Point(r.p,0)), r.getSet(Point(i,j)));
            }
        }
    }
    return r;
}


//does the coarsening
//takes the list of relations which came from ramification
//gives back a list of relations, where the final relations are saved in
list<Relation> coarsening(list<Relation> &worklist)
{
    list<Relation> endlist;   //the list where the final relations are saved, this will be returned in the end

    list<Relation>::iterator it=worklist.begin();
    while(!(worklist.empty()))
    {
        Point toDo(0,0);   //the Points which belong to no equivalenceclass
        int i=1;
        int j=1;
        while(i<=it->p)
        {
            while(j<=it->p)
            {
                if(it->getClass(Point(i,j)) == 100)   //if it belongs to no equivalenceclass
                {
                    toDo=Point(i,j);
                    Relation r0 = close0(toDo, *it); // the relation where the point is included to the set of 0
                    Relation rA = closeA(toDo, *it); // the relation where the point is included to the set of alpha
                    r0.deleteEmpty();
                    rA.deleteEmpty();
                    worklist.push_back(r0);   //includes it in worklist
                    worklist.push_back(rA);   //includes it in worklist

                    it=worklist.erase(it);
                    toDo=Point(0,0);
                    i=1;
                    j=1;
                }
                else
                {
                    j++;
                    if((j>(it->p))&&(i!=(it->p)+1))
                    {
                       j=1;
                       i++;
                    }
                }
            }
        }
        if(toDo==Point(0,0))  //if there is no Point left which belongs to no equivalenceclass, i.e. it is finished
        {
            endlist.push_back(*it);   //it is inserted in endlist
            it=worklist.erase(it);  //erases the relation (it's not needed any more) and sets the iterator to the next element
        }
    }
    return endlist;
}


void printallCoextensions(TomoMath t_m)
{
    cout<< "The underlying tomonoid is:"<<endl;
    t_m.write();
    cout<<endl;
    cout<< "and its one-element coextensions are given by:"<<endl;
    list<TomoMath> final=t_m.getCoextensions();
    list<TomoMath>::iterator it;
    for(it=final.begin(); it!=final.end(); it++)
    {
        cout<<"With idempotents ("<< it->id_x << ","<< it->id_y<< ")"<< endl;
        it->write();
        cout<<endl;
    }
    cout << "Done." << endl;
}






/* point.h
 *
 * Copyright 2015-2017 Johannes Kepler University Linz,
 * Department of Knowledge Based Mathematical Systems,
 * author: Laura Peham <l.peham@hotmail.com>.
 *
 * This file is part of finite-negative-tomonoids.
 *
 * finite-negative-tomonoids is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * finite-negative-tomonoids is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with finite-negative-tomonoids. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef POINT
#define POINT


// The class Point represents a 2-dimensional integer Point
// The members are: int x, int y

class Point
{
public:
    int x;   //the first coordinate of the point
    int y;   //the second coordinate of the point

    Point();  //Default Constructor
    Point(int a, int b);  //Constructor

    bool operator<(Point a) const ;  //checks, if point a is bigger than this point
    bool operator==(Point a) const;  //compares two Points
};

#endif // POINT


/* tomocode.h
 *
 * Copyright 2015-2017 Johannes Kepler University Linz,
 * Department of Knowledge Based Mathematical Systems,
 * author: Laura Peham <l.peham@hotmail.com>.
 *
 * This file is part of finite-negative-tomonoids.
 *
 * finite-negative-tomonoids is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * finite-negative-tomonoids is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with finite-negative-tomonoids. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TOMOCODE_H
#define TOMOCODE_H

#include <iostream>
#include <vector>
#include <point.h>
using namespace std;

class TomoMath;
class Relation;


// The class TomoCode represents a Tomonoid in the intern form, is constructed from a TomoMath
// This means that now we don't have letters anymore, so now we name the elements from 0,1,2,...,n-1
// Where now 0 is the biggest element (original 1) and so on. So what was original 0 is now n-1

class TomoCode
{
public:
    int n;   //the size of the tomonoid, i.e. the number of elements
    vector<int> tom;   //the tomonoid itself
    Point id;   // after the coextension, this is the used (el, er)

    TomoCode();  //Default Constructor
    TomoCode(TomoMath t_m);  //Constructor
    TomoCode(Relation sets);  //Constructor
    ~TomoCode();  //Destructor

    TomoCode extend();  //returns a new tomonoid, which is the zero doubling extension

    bool operator==(TomoCode const &a) const;  //compares two TomoCodes
    bool operator<(TomoCode const &a) const;//compares two TomoCodes
};

#endif // TOMOCODE_H

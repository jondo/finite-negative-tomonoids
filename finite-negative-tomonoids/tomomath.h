/* tomomath.h
 *
 * Copyright 2015-2017 Johannes Kepler University Linz,
 * Department of Knowledge Based Mathematical Systems,
 * author: Laura Peham <l.peham@hotmail.com>.
 *
 * This file is part of finite-negative-tomonoids.
 *
 * finite-negative-tomonoids is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * finite-negative-tomonoids is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with finite-negative-tomonoids. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TOMOMATH_H
#define TOMOMATH_H

#include"tomocode.h"
#include<iostream>
#include<list>
#include<vector>
#include <point.h>
using namespace std;


//The class TomoMath represents a tomonoid in the known mathematical way
// The elements are saved line to line in the vecto<char> tom, the elements are given by 1,z,y,x,...,0

class TomoMath
{
public:
    int n;     //the size of the tomonoid, i.e. the number of elements
    vector<char> tom;    //the tomonoid itself
    char id_x;   //after the coextension, this is the used el
    char id_y;   //after the coextension, this is the used el

    TomoMath();  //Default Constructor
    TomoMath(int number);  //Constructor for user defined tomonoid with size number
    TomoMath(TomoCode t_c);  //Constructor
    ~TomoMath();  //Destructor

    void allocateNew(int n);  //allocates the array with size n
    void input(const char* filename);  //reads in the tomonoid from a given file
    void write();  //prints out the tomonoid

    list<TomoMath> getCoextensions();  //the final function in the programm
};

#endif // TOMOMATH_H
